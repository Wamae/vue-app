import Vue from "vue";
import axios from "axios";
import Form from "../../js/core/Form";
import Example from "../../js/components/Example";


window.axios = axios;
window.Form = Form;

new Vue({
    el: "#app",
    components:{
        Example
    },
    data: {
        myForm: new Form({
            name: "",
            description: "",
        })
    },
    methods: {
        onSubmit() {
            this.myForm.post("/projects")
                .then(data=>alert("Handling it!"))
                .catch(error=>console.log(error));
        }
    }
});
