<html>
<head>
    <title></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
    <style>
        body {
            padding-top: 40px;
        }
    </style>
</head>
<body>
<div id="app" class="container">
    @include('projects.list')

    <example></example>
    <hr>
    <form method="POST" action="/projects" @submit.prevent="onSubmit" @keydown="myForm.errors.clear($event.target.name)">
        <div class="control">
            <label for="name" class="label">Project Name: </label>
            <input type="text" id="name" name="name" class="input" v-model="myForm.name">

            <span class="help is-danger" v-if="myForm.errors.has('name')" v-text="myForm.errors.get('name')"></span>
        </div>

        <div class="control">
            <label for="description" class="label">Project Description: </label>
            <input type="text" id="description" name="description" class="input" v-model="myForm.description">
            <span class="help is-danger" v-if="myForm.errors.has('description')" v-text="myForm.errors.get('description')"></span>
        </div>

        <div class="control">
            <button class="button is-primary" :disabled="myForm.errors.any()">Create</button>
        </div>

    </form>
</div>
</body>

<script src="/js/app.js" ></script>
</html>
